import csv
import numpy as np
import scipy
from sklearn import tree
from sklearn.naive_bayes import BernoulliNB
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import KFold
from sklearn.externals import joblib

#method to calculate error
def errorRate(model,validationSet):
    bad = 0;
    for i in validationSet:
        if (model.predict([i[:-1]]) != i[-1]):
            bad = bad + 1;

    # print(bad)
    # print(len(list(validationSet)))
    totalLines = len(list(validationSet))
    return 100 - (bad/totalLines) * 100

#loads from model and writes-decision tree
def writedt():
    filetest = np.genfromtxt("ds1Test.csv",delimiter=",")
    fileval = np.genfromtxt("ds1Val.csv",delimiter=",")
    filetest2 = np.genfromtxt("ds2Test.csv",delimiter=",")
    fileval2 = np.genfromtxt("ds2Val.csv",delimiter=",")

    dt = joblib.load('dt-dataset1.joblib')
    dt2 = joblib.load('dt-dataset2.joblib')

    writeTest(dt,filetest,"ds1Test-dt.csv")
    writeVal(dt,fileval,"ds1Val-dt.csv")
    writeTest(dt2,filetest2,"ds2Test-dt.csv")
    writeVal(dt2,fileval2,"ds2Val-dt.csv")
    # print('ACCURACY RATE DT TREE (validation,ds1): ',
    #       errorRate(dt, fileval), ' %')
    #
    # print('ACCURACY RATE DT TREE (validation,ds2): ',
    #       errorRate(dt2, fileval2), ' %')

#loads from model and writes_naive bayes
def writenb():
    filetest = np.genfromtxt("ds1Test.csv", delimiter=",")
    fileval = np.genfromtxt("ds1Val.csv", delimiter=",")
    filetest2 = np.genfromtxt("ds2Test.csv", delimiter=",")
    fileval2 = np.genfromtxt("ds2Val.csv", delimiter=",")

    nb = joblib.load('nb-dataset1.joblib')
    nb2 = joblib.load('nb-dataset2.joblib')

    writeTest(nb, filetest, "ds1Test-nb.csv")
    writeVal(nb, fileval, "ds1Val-nb.csv")
    writeTest(nb2, filetest2, "ds2Test-nb.csv")
    writeVal(nb2, fileval2, "ds2Val-nb.csv")
    #
    # print('ACCURACY RATE BAYES TREE (validation,ds1): ', errorRate(nb,fileval), ' %')
    #
    # print('ACCURACY RATE BAYES TREE (validation,ds2): ', errorRate(nb2,fileval2), ' %')

#loads from model and writes neural network
def writenn():
    filetest = np.genfromtxt("ds1Test.csv", delimiter=",")
    fileval = np.genfromtxt("ds1Val.csv", delimiter=",")
    filetest2 = np.genfromtxt("ds2Test.csv", delimiter=",")
    fileval2 = np.genfromtxt("ds2Val.csv", delimiter=",")

    nn = joblib.load('nn-dataset1.joblib')
    nn2 = joblib.load('nn-dataset2.joblib')

    writeTest(nn, filetest, "ds1Test-3.csv")
    writeVal(nn, fileval, "ds1Val-3.csv")
    writeTest(nn2, filetest2, "ds2Test-3.csv")
    writeVal(nn2, fileval2, "ds2Val-3.csv")
    #
    # print('ACCURACY RATE nn (validation,ds1): ', errorRate(nn,fileval), ' %')
    #
    # print('ACCURACY RATE nn (validation,ds2): ', errorRate(nn2,fileval2), ' %')


#method to output validation
def writeVal(model, validationSet,name):
    file = open(name, "w")
    for index,i in enumerate(validationSet):
        file.write(str(index))
        file.write(",")
        prediction = int(model.predict([i[:-1]])[0])
        file.write(str(prediction))
        file.write("\n")
        # print(int(model.predict([i[:-1]])[0]))

#method to output test
def writeTest(model, validationSet,name):
    file = open(name, "w")
    for index,i in enumerate(validationSet):
        # print(i)
        file.write(str(index))
        file.write(",")
        prediction = int(model.predict([i])[0])
        file.write(str(prediction))
        file.write("\n")
        # print(int(model.predict([i])[0]))


def main():
    writedt()
    writenb()
    writenn()
    print("Classifcation Done. Please check excel files.")

    # decision tree classifier
    # file = np.genfromtxt("ds1test.csv",delimiter=",")
    # fileVal = np.genfromtxt("ds1Val.csv",delimiter=",")

    # file2 = np.genfromtxt("ds2Train.csv", delimiter=",")
    # fileVal2 = np.genfromtxt("ds2Val.csv", delimiter=",")

    # last = file[:,-1]
    # rest = file[:,:-1]
    # print(file[0])
    # last2 = file2[:, -1]
    # rest2 = file2[:, :-1]
    #
    # X = rest
    # Y = last
    #
    # X2 = rest2
    # Y2 = last2

    # Decision Tree:
    # dt = tree.DecisionTreeClassifier(criterion="entropy", splitter="random", max_depth=80, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features=None, random_state=None, max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, class_weight=None, presort=False)
    # dt = dt.fit(X,Y)
    #
    # dt2 = tree.DecisionTreeClassifier(criterion="entropy",
    #                                  splitter="random",
    #                                  max_depth=80,
    #                                  min_samples_split=2,
    #                                  min_samples_leaf=1,
    #                                  min_weight_fraction_leaf=0.0,
    #                                  max_features=None,
    #                                  random_state=None,
    #                                  max_leaf_nodes=None,
    #                                  min_impurity_decrease=0.0,
    #                                  min_impurity_split=None,
    #                                  class_weight=None,
    #                                  presort=False)
    # dt2 = dt2.fit(X2, Y2)

    # Naive Bayes:
    # nb = BernoulliNB(alpha=0.5, binarize=0.0, class_prior=None, fit_prior=False)
    #
    # nb.fit(X,Y)
    #
    # nb2 = BernoulliNB(alpha=0.5, binarize=0.0, class_prior=None, fit_prior=False)
    #
    # nb2.fit(X2, Y2)

    # Neural Network:
    # kf = KFold(n_splits=5)


    #
    # nn = MLPClassifier(activation="relu", alpha=1e-05, batch_size="auto",
    #               beta_1=0.9, beta_2=0.999, early_stopping=False,
    #               epsilon=1e-08, hidden_layer_sizes=(700,),
    #               learning_rate="constant", learning_rate_init=0.01,
    #               max_iter=200, momentum=0.9, n_iter_no_change=10,
    #               nesterovs_momentum=True, power_t=0.5, random_state=1,
    #               shuffle=True, solver="lbfgs", tol=0.0001,
    #               validation_fraction=0.1, verbose=False, warm_start=False)
    #
    # nn2 = MLPClassifier(activation="relu", alpha=1e-05,
    #                    batch_size="auto",
    #                    beta_1=0.9, beta_2=0.999,
    #                    early_stopping=False,
    #                    epsilon=1e-08, hidden_layer_sizes=(700,),
    #                    learning_rate="constant",
    #                    learning_rate_init=0.01,
    #                    max_iter=200, momentum=0.9,
    #                    n_iter_no_change=10,
    #                    nesterovs_momentum=True, power_t=0.5,
    #                    random_state=1,
    #                    shuffle=True, solver="lbfgs", tol=0.0001,
    #                    validation_fraction=0.1, verbose=False,
    #                    warm_start=False)

    # for train_indices, test_indices in kf.split(X):
    #     dt.fit(X[train_indices], Y[train_indices])
    #     print(dt.score(X[test_indices], Y[test_indices]))


    # nn.fit(X,Y)
    # nn2.fit(X2,Y2)

    # write(nb,fileVal);
    # joblib.dump(nb, 'naiveBayes.joblib')

    # joblib.dump(dt, 'dt-dataset1.joblib')
    # joblib.dump(dt2, 'dt-dataset2.joblib')
    # joblib.dump(nb, 'nb-dataset1.joblib')
    # joblib.dump(nb2, 'nb-dataset2.joblib')
    # joblib.dump(nn, 'nn-dataset1.joblib')
    # joblib.dump(nn2, 'nn-dataset2.joblib')
    # clf = joblib.load('naiveBayes.joblib')
    # print(clf.predict([rest[0]]))
    # print('ERROR RATE NAIVE BAYES: ', errorRate(nb,fileVal), ' %')
    # print('ERROR RATE NEURALNETWORK: ', errorRate(nn,fileVal), ' %')
    # print('ACCURACY RATE DECSCION TREE (validation,ds1): ', errorRate(nn,fileVal), ' %')
    # print('ACCURACY RATE DECSCION TREE (Test,ds1): ', errorRate(nn,file), ' %')

    # print('ACCURACY RATE DECSCION TREE (validation,ds2): ', errorRate(dt,fileVal2), ' %')
    # print('ACCURACY RATE DECSCION TREE (Test,ds2): ', errorRate(dt,file2), ' %')

if __name__ == "__main__":
    main()
